<?php

use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class UserDefinedFormExtension extends DataExtension 
{

    private static $db = [
        'ExtraContent' => 'HTMLText'
    ];

    public function updateCMSFields(FieldList $fields) 
    {
        $fields->addFieldToTab("Root.Right Column", new HTMLEditorField("ExtraContent", "On UserForm Pages (ie contact or Immigration form pages) if you fill this out it will show on the right hand side of the page."));
        $fields->addFieldToTab("Root.EmailNotes", new LiteralField("EmailNotes", "Emails will be sent to the logged in user at time of filling out the form but note that the configuration of this page requires an email address to be added for it to work. As such the Recipient field for this form has a generic email address. Users should check the email address assigned to their login in order to view emails received from this form."));
    }

}
