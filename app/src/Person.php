<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class Person extends DataObject {

    private static $table_name = 'People';
    private static $default_sort = 'SortOrder';
    private static $has_one = [
        "Page" => SiteTree::class,
        "Image" => Image::class
    ];
	private static $db = [
        'PersonName'=>'Varchar',
        'Title' => 'Varchar',
        'Region' => 'Varchar',
        "Phone" => 'Varchar',
        "Email" => 'Varchar',
        'SortOrder' => 'Int'
    ];

    private static $owns = [
        'Page',
        'Image'
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create('PersonName', 'Name'),
            TextField::create('Title', 'Title'),
            TextField::create('Region', 'Regions'),
            TextField::create('Phone', 'Phone'),
            TextField::create('Email', 'Email'),
            UploadField::create('Image', 'Image')
        );
    }
}
