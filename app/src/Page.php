<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Assets\Image;
	use SilverStripe\Forms\GridField\GridField;
	use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use UndefinedOffset\SortableGridField\Forms\GridFieldSortableRows;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\TextField;

    class Page extends SiteTree
    {
        private static $db = [

            "HeadingOneAlt" => 'Varchar',
            "SubHeadingOne" => 'Varchar'
        ];

        private static $has_one = [
        	'ContentBlockOneImage' => Image::class,
        ];

        private static $has_many = [
            "Slides" => Slide::class,
        ];

        private static $owns = [
            'ContentBlockOneImage',
            'Slides',
        ];

        public function getCMSFields()
		{
			
			$fields = parent::getCMSFields();
			
            $fields->addFieldToTab("Root.ContentBlockOne", new UploadField("ContentBlockOneImage", "ContentBlockOneImage"));
            $fields->addFieldToTab("Root.ContentBlockOne", new TextField("HeadingOneAlt", "HeadingOneAlt"));
            $fields->addFieldToTab("Root.ContentBlockOne", new TextField("SubHeadingOne", "SubHeadingOne"));
            $confSlides = GridFieldConfig_RecordEditor::create(10);
            $confSlides->addComponent(new GridFieldSortableRows('SortOrder'));
            $fields->addFieldToTab('Root.Slides', new GridField('Slides', 'Slides', $this->Slides(), $confSlides));
            
			return $fields;
		}
    }
}
