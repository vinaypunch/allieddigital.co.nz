<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class Slide extends DataObject {

    private static $table_name = 'Slides';

    private static $default_sort = 'SortOrder';

    private static $has_one = [
        "Page" => SiteTree::class,
        "Image" => Image::class
    ];
    
	private static $db = [
        'HeadingOneAlt' => 'Varchar',
        'SubHeadingOne' => 'HTMLText',
        'SlideLink' => 'Text',
        'SortOrder' => 'Int',
        'Black' => 'Boolean'
    ];

    private static $owns = [
        'Page',
        'Image'
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create('HeadingOneAlt', 'Title to be shown on the slide'),
            TextField::create('SubHeadingOne', 'Sub Heading'),
            TextField::create('SlideLink', 'Link'),
            UploadField::create('Image', 'Background image used for this block'),
            CheckboxField::create('Black', 'When checked the H1 and H3 will be black')
        );
    }
}
