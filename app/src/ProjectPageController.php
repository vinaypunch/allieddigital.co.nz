<?php

namespace {

	use PageController;
	use SilverStripe\ORM\ArrayList;
	use SilverStripe\View\ArrayData;

	class ProjectPageController extends PageController
	{
		public function showServices() 
        {
			$replacearray = array("[","]","\"");
            $services = explode(',' , $this->ServiceProvided);
            $servicelist = new ArrayList();
            foreach($services as $item) {
				$formatservicetitle = str_replace($replacearray,"",$item);
                $servicelist->push(
                    new ArrayData(array('services' => $formatservicetitle))
                );
            }
            return $servicelist;
        }
	}

}