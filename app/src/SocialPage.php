<?php

namespace {

    use SilverStripe\AssetAdmin\Forms\UploadField;
	use SilverStripe\Assets\Image;
	use SilverStripe\Forms\GridField\GridField;
	use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use UndefinedOffset\SortableGridField\Forms\GridFieldSortableRows;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\TextField;

    class SocialPage extends HomePage
    {
        private static $db = [

            "HeadingOneAlt" => 'Varchar',
            "SubHeadingOne" => 'Varchar',
            "HeadingTwo" => 'Varchar',
            "ContentTwo" => 'HTMLText',
            "HeadingThree" => 'Varchar',
            "ContentThree" => 'HTMLText',
            "HeadingFour" => 'Varchar',
            "ContentFour" => 'HTMLText',
            "HeadingFive" => 'Varchar',
            "ContentFive" => 'HTMLText'
        ];

        private static $has_one = [
        	'ContentBlockOneImage' => Image::class,
        	'ContentBlockTwoImage' => Image::class,
        	"ContentBlockThreeImage" => Image::class,
        	"ContentBlockFourImage"  => Image::class,
        	"ContentBlockFiveImage"  => Image::class
        ];

        private static $has_many = [
        	'ContentBlockTwoIcons' => ContentBlockTwoIcon::class,
        	"People" => Person::class,
        ];
        

        private static $owns = [
            'ContentBlockOneImage',
            'ContentBlockTwoImage',
            'ContentBlockThreeImage',
            "ContentBlockTwoIcons",
            "ContentBlockFourImage",
            "ContentBlockFiveImage",
            "People"
        ];

        public function getCMSFields()
		{
			
			$fields = parent::getCMSFields();
			
            $fields->removeByName('ContentBlockTwoIcons');
            $fields->removeByName('People');

            $fields->addFieldToTab("Root.ContentBlockOne", new UploadField("ContentBlockOneImage", "ContentBlockOneImage"));
            $fields->addFieldToTab("Root.ContentBlockOne", new TextField("HeadingOneAlt", "HeadingOneAlt"));
            $fields->addFieldToTab("Root.ContentBlockOne", new TextField("SubHeadingOne", "SubHeadingOne"));

            $fields->addFieldToTab("Root.ContentBlockTwo", new UploadField("ContentBlockTwoImage", "ContentBlockTwoImage"));
            $fields->addFieldToTab("Root.ContentBlockTwo", new TextField("HeadingTwo", "HeadingTwo"));
            $fields->addFieldToTab("Root.ContentBlockTwo", new HTMLEditorField("ContentTwo", "ContentTwo"));

            $fields->addFieldToTab("Root.ContentBlockThree", new UploadField("ContentBlockThreeImage", "ContentBlockThreeImage"));
            $fields->addFieldToTab("Root.ContentBlockThree", new TextField("HeadingThree", "HeadingThree"));
            $fields->addFieldToTab("Root.ContentBlockThree", new HTMLEditorField("ContentThree", "ContentThree"));

            $fields->addFieldToTab("Root.ContentBlockFourImage", new UploadField("ContentBlockFourImage", "ContentBlockFourImage"));
            $fields->addFieldToTab("Root.ContentBlockFourImage", new TextField("HeadingFour", "HeadingFour"));
            $fields->addFieldToTab("Root.ContentBlockFourImage", new HTMLEditorField("ContentFour", "ContentFour"));

            $fields->addFieldToTab("Root.ContentBlockFive", new UploadField("ContentBlockFiveImage", "ContentBlockFiveImage"));
            $fields->addFieldToTab("Root.ContentBlockFive", new TextField("HeadingFive", "HeadingFive"));
            $fields->addFieldToTab("Root.ContentBlockFive", new HTMLEditorField("ContentFive", "ContentFive"));

			return $fields;
		}
    }
}
