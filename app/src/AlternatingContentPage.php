<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\AssetAdmin\Forms\UploadField;
	use SilverStripe\Assets\Image;
	use SilverStripe\Forms\GridField\GridField;
	use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use UndefinedOffset\SortableGridField\Forms\GridFieldSortableRows;

    class AlternatingContentPage extends Page
    {
        private static $db = [];

        private static $has_one = [];

        private static $has_many = [
        	'ContentBlocks' => ContentBlock::class,
        ];

        private static $owns = [
            'ContentBlocks',
        ];

        public function getCMSFields()
		{
			
			$fields = parent::getCMSFields();
            
            $conf = GridFieldConfig_RecordEditor::create(10);
            $conf->addComponent(new GridFieldSortableRows('SortOrder'));
            $fields->addFieldToTab('Root.ContentBlocks', new GridField('ContentBlocks', 'ContentBlockTwoIcons', $this->ContentBlocks(), $conf));

			return $fields;
		}
    }
}
