<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class CustomSiteConfig extends DataExtension 
{
    
    private static $db = [
        'Phone' => 'Text',
        'Fax' => 'Text',
        'Email' => 'Text',
        'AddressOne' => 'Text',
        'AddressTwo' => 'Text',
        'FooterColumnOne' => 'HTMLText',
        'FooterColumnTwo' => 'HTMLText',
        //"FooterColumnThree" => 'HTMLText'
    ];

    private static $has_one = [
        'Logo' => Image::class,
        'ScrollLogo' => Image::class
    ];

    private static $owns = [
        'Logo',
        'ScrollLogo'
    ];

    public function updateCMSFields(FieldList $fields) 
    {
        $fields->addFieldToTab("Root.Logo", new UploadField("Logo", "Logo displayed when you have a transparent or non-white background"));
        $fields->addFieldToTab("Root.Logo", new UploadField("ScrollLogo", "Logo displayed when you have a white background"));
        $fields->addFieldToTab("Root.Details", new TextField("Email", "Email"));
        $fields->addFieldToTab("Root.Details", new TextField("AddressOne", "AddressOne"));
        $fields->addFieldToTab("Root.Details", new TextField("AddressTwo", "AddressTwo"));
        $fields->addFieldToTab("Root.Details", new HTMLEditorField("FooterColumnOne", "FooterColumnOne"));
        $fields->addFieldToTab("Root.Details", new HTMLEditorField("FooterColumnTwo", "FooterColumnTwo"));
        //$fields->addFieldToTab("Root.Details", new HTMLEditorField("FooterColumnThree", "FooterColumnThree"));
    }
}