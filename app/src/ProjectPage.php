<?php

namespace {

	//use Page;    

	use SilverStripe\Forms\TextField;
	use SilverStripe\Forms\ListboxField;
	use SilverStripe\Assets\Image;
	use SilverStripe\AssetAdmin\Forms\UploadField;

	class ProjectPage extends Page
	{
		private static $table_name = 'ProjectPage';
		private static $can_be_root = false;

		private static $db = [
			'ShortIntro' => 'Text',
			'ServiceProvided' => 'Varchar',
			'ProjectClient'=>'Text'
		];

		private static $has_one = [
			'ProjectImage' => Image::class
		];

		 private static $owns = [
			'ProjectImage'
		];

		public function getCMSFields()
		{

			$fields = parent::getCMSFields();
			$fields->removeByName('ContentBlockOne');
			$fields->removeByName('Slides');
			$fields->addFieldToTab('Root.Main', TextField::create('ShortIntro','Short Intro'),'Content');
			$fields->addFieldToTab('Root.Main', TextField::create('ProjectClient','Client'),'Metadata');
			$fields->addFieldToTab('Root.Main', ListboxField::create('ServiceProvided','Service/s Provided',array("Google Ads"=>"Google Ads","Social Media"=>"Social Media","Video Production"=>"Video Production","Content Creation"=>"Content Creation","Website Development"=>"Website Development","Photography"=>"Photography","News Network Advertising"=>"News Network Advertising","Homepage Takeover"=>"Homepage Takeover","Print"=>"Print","Pre-Roll"=>"Pre-Roll","Sponsored Content"=>"Sponsored Content")),'Metadata');
			$fields->addFieldToTab('Root.Main', UploadField::create('ProjectImage', 'Image')
				->setFolderName('projects')
				->setAllowedExtensions(array('jpg','jpeg'))
				->setDescription('Image size: 800x600px'),'Metadata');

			return $fields;
		}

	}
}