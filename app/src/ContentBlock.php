<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class ContentBlock extends DataObject {

    private static $table_name = 'ContentBlock';
    private static $default_sort = 'SortOrder';
    private static $has_one = [
        "Page" => SiteTree::class,
        "Image" => Image::class
    ];
    
	private static $db = [
        'Title' => 'Varchar',
        'Content' => 'HTMLText',
        'SortOrder' => 'Int',
        //'LinkText'  => 'Varchar',
        //'Link'  => 'Varchar',
        'Remove' => 'Boolean',
    ];

    private static $owns = [
        'Page',
        'Image'
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create('Title', 'Title to be shown on page'),
            HTMLEditorField::create('Content', 'Description of Content'),
            //TextField::create('LinkText', 'Link Text'),
            //TextField::create('Link', 'Page URL to be used with the link text (internal links only e.g. about)'),
            UploadField::create('Image', 'Background image used for this block'),
            CheckboxField::create('Remove', 'When chekced will remove this content block from public view')
        );
    }
}
