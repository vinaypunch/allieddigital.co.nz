<?php 

namespace {
	
	class ProjectHolder extends Page
	{
		private static $table_name = 'ProjectHolder';

		private static $allowed_children = [
			ProjectPage::class
		];
		
		public function getCMSFields()
		{

			$fields = parent::getCMSFields();
			$fields->removeByName('ContentBlockOne');
			return $fields;
		}
	}
}