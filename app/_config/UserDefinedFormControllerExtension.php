<?php

use SilverStripe\ORM\DataExtension;
use SilverStripe\Security\Security;

class UserDefinedFormControllerExtension extends DataExtension 
{

    public function updateEmail($email, $recipient, $emailData) 
    {	
    	$member = Security::getCurrentUser();
        return $email->setSubject('(' . $member->Name . ')' . ' - form submission from allieddigital.co.nz');
    }

}
